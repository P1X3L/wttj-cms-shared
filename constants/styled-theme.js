const theme = {}

theme.colors = {
  white: '#FFFFFF',
  whiteRGBA: '255,255,255',
  black: '#000000',
  blackRGBA: '0,0,0',
  primary: '#00C29A',
  primaryRGBA: '0,194,154',
  green: [
    '#ebfff6',
    '#ebfff6',
    '#2fd38e',
    '#baf4d9'
  ],
  red: [
    '#ffebee',
    '#e57373',
    '#d32f2f'
  ],
  blue: [
    '#ebf9ff',
    '#baddf4',
    '#2f97d3'
  ],
  orange: [
    '#fffbeb',
    '#f4d1ba',
    '#f48c23'
  ],
  gray: [
    '#F9F9F9',
    '#E6E6E6',
    '#CFCFCF',
    '#97999D',
    '#87888D',
    '#4B4D55',
    '#41434B',
    '#373942',
    '#31333B',
    '#2A2C31'
  ],
  linkedin: [
    '#007bb6',
    '#006ea3'
  ],
  texts: {
    xlight: '#CFCFCF',
    light: '#97999D',
    dark: '#373942',
    error: '#f44336',
  },
  universes: {
    tech: '#00C29A',
    consulting: '#6495d6',
    fashion: '#cfac77',
    food: '#d65c68'
  }

}

theme.fontSizes = {
  html: '16px',
  body: '1rem',
  xxs: '.7rem',
  xs: '.8rem',
  sm: '.9rem',
  md: '1rem',
  lg: '1.1rem',
  xl: '1.3rem',
  xxl: '1.8rem'
}

theme.gutters = {
  xxs: '.3rem',
  xs: '.5rem',
  sm: '.8rem',
  md: '1rem',
  lg: '1.6rem',
  mdx2: '2rem',
  xl: '3.2rem',
  xxl: '4rem',
  xxxl: '6.9rem'
}

theme.fontFamilies = {
  texts: 'HKCompakt',
  icons: 'Material-design-iconic-font'
}

theme.fontSizes = {
  xs: '.7rem',
  sm: '.8rem',
  md: '1rem',
  lg: '1.2rem',
  xl: '1.4rem',
  xxl: '2rem'
}

theme.fontWeights = {
  light: '300',
  regular: '400',
  bold: '600',
  black: '700'
}

theme.transitions = {
  sm: 'all .2s ease',
  md: 'all .3s ease',
  lg: 'all 1s ease'
}

theme.centeredContainerWidths = {
  sm: '40rem',
  md: '56rem',
  lg: '78rem'
}

theme.searchFormWidths = {
  sm: '54rem',
  md: '68.75rem',
  lg: '78rem'
}

theme.headerHeight = '13.6rem'
theme.headerMobileHeight = '8rem'
theme.headerUpperNavHeight = '2.6rem'
theme.searchFormHeight = '3.75rem'
theme.universeHeight = '17.5rem'

theme.bannerHeights = {
  article: '30rem',
  home: '47rem',
  job: '35rem',
  search: '17.6rem',
  interview: '30rem',
  magazines: '35rem',
  settings: '32rem',
  static_pages: '24rem',
  mobile: {
    home: '38rem',
    job: '28rem',
    article: '15rem',
    interview: '20rem',
    magazines: '28rem',
  }
}

theme.imagesHeights = {
  organizations: {
    show: {
      banner: '29rem',
      logo: '12.5rem'
    },
    thumb: {
      cover: '9.4rem'
    }
  },
  articles: {
    index: {
      banner: '16rem'
    },
    show: {
      banner: '30rem'
    }
  },
  jobs: {
    thumb: {
      logo: '4rem'
    }
  }
}

theme.shareButtonSizes = {
  sm: 16,
  md: 32,
  lg: 46
}

theme.buttonIconWidth = '2.9rem'

theme.coverHeights = {
  default: '13.6rem'
}

theme.modalSizes = {
  sm: '30rem',
  md: '50rem',
  lg: '60rem',
}

theme.radius = {
  sm: '2px',
  md: '5px',
  lg: '10px',
}

theme.boxShadows = {
  buttons: '0 2px 4px rgba(0,0,0,.2)',
  xs: '0 1px 2px rgba(0,0,0,.1)',
  sm: '0 2px 2px rgba(0,0,0,.1)',
  smTop: '0 -2px 2px rgba(0,0,0,.1)',
  md: '0 3px 10px rgba(0,0,0,.1)',
  lg: '0 4px 15px rgba(0,0,0,.2)',
  xl: '0 8px 20px rgba(0,0,0,.2)',
}

theme.breakpoints = {
  mediumscreen: '1300px',
  smallscreen: '1100px',
  tablet: '900px',
  mobile: '600px'
}

export default theme
