import React, {Component} from 'react'
import PropTypes from 'prop-types'

import Section from './section'
import Navigation from './navigation'

class Page extends Component {

  render() {
    const { page, pages, sectionComponent, organization } = this.props
    const SectionComponent = sectionComponent
    return (
      <div>
        {organization &&
          <Navigation
            page={page}
            pages={pages}
            organization={organization}
          />
        }
        {page.get('sections').map((section, i) =>
          <SectionComponent
            key={i}
            section={section}
            containers={section.get('containers')}
          />
        )}
      </div>
    )
  }
}

Page.propTypes = {
  sectionComponent: PropTypes.func
}

Page.defaultProps = {
  sectionComponent: Section
}

export default Page
