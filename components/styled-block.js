import styled from 'styled-components'

const Block = styled.div`
  overflow: hidden;
  box-sizing: border-box;
  padding: 10px;
  background: lightgrey;
  border: 1px solid black;
`

export default Block
