import styled from 'styled-components'

const Container = styled.div`
  box-sizing: border-box;
  border: 1px solid black;
  flex: ${props => {
    const { layoutContainer } = props
    if (
      layoutContainer && layoutContainer.get('width')
      && layoutContainer.get('width') !== 'auto'
    ) {
      return `0 0 ${layoutContainer.get('width')}`
    } else {
      return '1 1 auto'
    }
  }}
`

export default Container
