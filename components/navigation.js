import React, {Component} from 'react'
import { Link } from 'react-router-dom'
import path from 'path'

import CmsLink from './link'

class PageItem extends Component {
  pageUrl(page) {
    const { organization } = this.props
    return path.join('/companies', organization.get('slug'), page.get('slug'))
  }

  sortedPages() {
    const { pages, page } = this.props
    return pages.filter(p => p.get('parent_id') === page.get('id')).sortBy(p => p.get('position'))
  }

  render() {
    const { page, pages, level, websiteReference, organizationReference } = this.props
    const children = this.sortedPages()
    return (
      <li>
        <Link
          to={this.pageUrl(page)}
        >
          {level === 1 && "|-->"}
          {page.get('name')}
        </Link>
        {children.size > 0 &&
          <ul>
            {children.map(child =>
              <PageItem
                key={child.get('id')}
                page={child}
                pages={pages}
                websiteReference={websiteReference}
                organizationReference={organizationReference}
                level={level + 1}
              />
            )}
          </ul>
        }
      </li>
    )
  }
}

class CmsNavigation extends Component {

  rootPages() {
    const { pages } = this.props
    return pages.filter(p => !p.get('parent_id')).sortBy(p => p.get('position'))
  }

  // FIXME render chldren with depth > 1
  render() {
    const { pages, organization } = this.props
    return (
      <nav
        style={{marginLeft: '300px'}}
      >
        <ul>
          {this.rootPages().map(page =>
            <PageItem
              key={page.get('id')}
              page={page}
              pages={pages}
              organization={organization}
              level={0}
            />
          )}
        </ul>
      </nav>
    )
  }
}

export default CmsNavigation
