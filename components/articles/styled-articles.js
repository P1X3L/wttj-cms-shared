import styled from 'styled-components'
import CmsLink from '../link'

export const Article = styled.article`
  position: relative;
  display: block;
  transition: ${props => props.theme.transitions.md};
  user-select: none;

  &:hover{
    transform: translateY(-10px);
  }

  &:active{
    transform: translateY(-5px);
  }
`

export const Cover = styled.div`
  position: absolute;
  z-index: 1;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  margin-bottom: ${props => props.theme.gutters.lg};
  background-color: ${props => props.theme.colors.gray[1]};
  box-shadow: ${props => props.theme.boxShadows.md};
  transition: ${props => props.theme.transitions.md};

  ${Article}:hover & {
    box-shadow: ${props => props.theme.boxShadows.lg};
  }

  &::before{
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    opacity: .3;
    background-color: ${props => props.theme.colors.black};
    content: " ";
  }
`

export const Title = styled.h3`
  margin-bottom: ${props => props.theme.gutters.sm};
  font-size: ${props => props.theme.fontSizes.lg};
  font-weight: ${props => props.theme.fontWeights.bold};
`

export const Author = styled.div`
  display: flex;
  align-items: center;
`

export const AuthorImg = styled.img`
  display: block;
  width: 25px;
  height: 25px;
  border-radius: ${props => props.theme.radius.sm};;
  overflow: hidden;
  background: ${props => props.theme.colors.gray[3]};
  margin-right: ${props => props.theme.gutters.xxs};
`

export const AuthorName = styled.span`
  font-size: ${props => props.theme.fontSizes.sm};
  font-weight: ${props => props.theme.fontWeights.light};
`

export const Actions = styled.div`
  position: absolute;
  top: ${props => props.theme.gutters.md};
  right: ${props => props.theme.gutters.md};
  display: flex;
  justify-content: flex-end;
`

export const ActionLink = styled(CmsLink)`
  display: flex;
  align-items: center;
  margin-left: ${props => props.theme.gutters.xs};

  svg{
    path{
      fill: ${props => props.theme.colors.white};
    }
  }
`