import styled from 'styled-components'
import CmsLink from '../link'
import { Article, Cover } from './styled-articles'

export const Link = styled(CmsLink)`
  color: ${props => props.theme.colors.white};
`

export const BannerCover = Cover.extend`
  position: absolute;
  z-index: 1;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`
export const Header = styled.header`
  max-width: 40rem;
`

export const Banner = Article.extend`
  height: 100%;
  border-radius: ${props => props.theme.radius.md};
  overflow: hidden;
  transition: ${props => props.theme.transitions.md};

  & > a {
    position: relative;
    z-index: 2;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    padding: ${props => props.theme.gutters.lg};
    height: 100%;
    color: ${props => props.theme.colors.white};
  }
`