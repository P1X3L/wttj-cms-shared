import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'

import {
  Thumb, ThumbCover, Link, Header, Content, Description
} from './styled-thumb'
import { Title, Author, AuthorImg, AuthorName } from './styled-articles'

class ArticlesThumb extends Component {Item
  name() {
    const { highlightedName, article } = this.props
    if (highlightedName) {
      return highlightedName
    }
    return article.get('name')
  }

  render() {
    const { article, highlightedName } = this.props
    return (
      <Thumb>
        <Link
          kind={'article'}
          values={{slug: article.get('slug')}}
        >
          <ThumbCover
            style={{backgroundImage: `url('${article.getIn(['image', 'thumb', 'url'])}')`}}
          />
          <Header>
            <Title>
              {this.name()}
            </Title>
            <Author>
              <AuthorImg
                src={article.getIn(['user', 'image', 'thumb', 'url'])}
                alt={article.getIn(['user', 'name'])}
              />
              <AuthorName>
                <FormattedMessage
                  id='articles.thumb.userName'
                  values={{ name: article.getIn(['user', 'name']) }}
                />
              </AuthorName>
            </Author>
          </Header>
          {article.get('content') &&
            <Content>
              <Description>
                {article.get('content')}
              </Description>
            </Content>
          }
        </Link>
      </Thumb>
    )
  }
}

export default ArticlesThumb
