import styled from 'styled-components'
import CmsLink from '../link'
import { Article, Cover, Title, AuthorName } from './styled-articles'

export const Thumb = Article.extend`
  ${Title}{
    color: ${props => props.theme.colors.gray[8]};
  }

  ${AuthorName}{
    color: ${props => props.theme.colors.gray[3]};
  }
`

export const Link = styled(CmsLink)`
  display: block;
`

export const ThumbCover = Cover.extend`
  position: relative;
  height: 180px;
  overflow: hidden;
  margin-bottom: ${props => props.theme.gutters.lg};
  border-radius: ${props => props.theme.radius.md};
`

export const Header = styled.header`
`

export const Content = styled.div`
`

export const Description = styled.p`
  font-weight: ${props => props.theme.fontWeights.light};
  font-size: ${props => props.theme.fontSizes.md};
  color: ${props => props.theme.colors.gray[4]};
`
