import React, { Component } from 'react'
import { FormattedMessage } from 'react-intl'

import {
  Banner, BannerCover, Link, Header
} from './styled-banner'
import { Cover, Title, Author, AuthorImg, AuthorName } from './styled-articles'

class ArticlesBanner extends Component {
  name() {
    const { highlightedName, article } = this.props
    if (highlightedName) {
      return highlightedName
    }
    return article.get('name')
  }

  render() {
    const { article, highlightedName, size } = this.props

    return (
      <Banner>
        <BannerCover
          style={{backgroundImage: `url('${article.getIn(['image', 'thumb', 'url'])}')`}}
        />
        <Link
          kind={'article'}
          values={{slug: article.get('slug')}}
        >
          <Header>
            <Title
              size={size}
            >
              {this.name()}
            </Title>
            <Author>
              <AuthorImg
                src={article.getIn(['user', 'image', 'thumb', 'url'])}
                alt={article.getIn(['user', 'name'])}
              />
              <AuthorName>
                <FormattedMessage
                  id='articles.thumb.userName'
                  values={{ name: article.getIn(['user', 'name']) }}
                />
              </AuthorName>
            </Author>
          </Header>
        </Link>
      </Banner>
    )
  }
}

export default ArticlesBanner
