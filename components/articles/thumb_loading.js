import React, { Component } from 'react'

import { Title, Author } from './styled-articles'
import {
  LoadingThumb, LoadingCover, LoadingHeader
} from './styled-thumb_loading'

import { LoadingTextLine } from 'components/styled-loading'

class ArticleThumbLoading extends Component {

  render() {
    return (
      <LoadingThumb
        to="#"
      >
        <LoadingCover />
        <LoadingHeader>
          <Title>
            <LoadingTextLine
              width="three_quarters"
              height="lg"
            />
          </Title>
          <Author>
            <LoadingTextLine
              width="half"
              height="sm"
            />
          </Author>
        </LoadingHeader>
      </LoadingThumb>
    )
  }
}

export default ArticleThumbLoading
