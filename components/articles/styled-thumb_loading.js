import styled from 'styled-components'

import { Thumb, ThumbCover, Header } from './styled-thumb'
import { loadingStyles } from 'components/styled-loading'

export const LoadingThumb = Thumb.extend`
  pointer-events: none;
`

export const LoadingCover = ThumbCover.extend`
  ${loadingStyles}

  &::before {
    display: none;
  }
`

export const LoadingHeader = Header.extend`
  margin-bottom: 0;
  padding-bottom: ${props => props.theme.gutters.lg};
`