import React, {Component} from 'react'

class CmsContentsCompanyStats extends Component {

  renderStat(key) {
    const { content } = this.props
    return (
      <div>
        <span>{content.getIn(['properties', key, 'label'])}</span>
        <span>{content.getIn(['properties', key, 'value'])}</span>
      </div>
    )
  }

  render() {
    return (
      <div>
        {this.renderStat('nb_employees')}
        {this.renderStat('parity_women')}
        {this.renderStat('parity_men')}
        {this.renderStat('average_age')}
        {this.renderStat('turnover')}
        {this.renderStat('revenue')}
      </div>
    )
  }
}

export default CmsContentsCompanyStats
