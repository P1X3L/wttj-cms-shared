import React, { Component } from 'react'
import Immutable from 'immutable'
import { InstantSearch, Hits, SearchBox, Pagination, Highlight, Configure } from 'react-instantsearch/dom'
import { FormattedRelative } from 'react-intl'

import CmsLink from '../link'

class JobHit extends Component {
  render() {
    const { hit } = this.props
    return (
      <ul>
        <li>
          <CmsLink
            kind={'job'}
            values={{job: Immutable.fromJS({organizationSlug: hit.organization.slug, slug: hit.slug})}}
          >
            <Highlight
              hit={hit}
              attributeName='name'
            />
          </CmsLink>
        </li>
        <li>
          {hit.contract_type_names.fr}
        </li>
        <li>
          <Highlight
            hit={hit}
            attributeName='office.city'
          />
        </li>
        <li>
          <FormattedRelative
            value={hit.published_at}
          />
        </li>
      </ul>
    )
  }
}

class CmsContentsJobsSearch extends Component {

  renderFilters() {
    const { content } = this.props
    if (!content.getIn(['properties','filters'])) { return null }
    return content.getIn(['properties','filters']).keySeq().map(k =>
      <Configure
        key={k}
        filters={`${k}:${content.getIn(['properties','filters', k])}`}
      />
    )
  }

  render() {
    const { content } = this.props
    return (
      <InstantSearch
        appId={content.getIn(['properties', 'algolia_app_id'])}
        apiKey={content.getIn(['properties', 'algolia_api_key'])}
        indexName={content.getIn(['properties', 'algolia_index_name'])}
      >
        {this.renderFilters()}
        <SearchBox />
        <Hits
          hitComponent={JobHit}
        />
        <Pagination />
      </InstantSearch>
    )
  }
}

export default CmsContentsJobsSearch
