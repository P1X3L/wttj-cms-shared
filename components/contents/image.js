import React, {Component} from 'react'

import { Image as StyledImage } from './styled-image'

class CmsContentsImage extends Component {

  alt() {
    const { content, organization } = this.props
    if (content.getIn(['properties', 'title'])) {
      return content.getIn(['properties', 'title'])
    } else if(organization) {
      return organization.get('name')
    }
  }

  title() {
    const { content } = this.props
    return content.getIn(['properties', 'title'])
  }

  subtitle() {
    const { content } = this.props
    return content.getIn(['properties', 'subtitle'])
  }

  content() {
    const { content } = this.props
    return content.getIn(['properties', 'content'])
  }

  thumbUrl() {
    const { content } = this.props
    let size = 'small'
    if (content.getIn(['properties', 'tags']) && content.getIn(['properties', 'tags']).includes('x3')) {
      size = 'large'
    }
    return content.getIn(['properties', 'image', size, 'url'])
  }

  open(e) {
    e && e.preventDefault()
    const { content, onOpenModal, organization } = this.props
    onOpenModal({
      content: (
        <div>
          <div>
            <img
              src={content.getIn(['properties', 'image', 'large', 'url'])}
              alt={this.alt()}
            />
          </div>
          <div>{organization && organization.get('name')}</div>
          <div>{this.title()}</div>
          <div>{this.content()}</div>
        </div>
      )
    })
  }

  render() {
    const title = this.title()
    const subtitle = this.subtitle()
    return (
      <a
        href='#'
        onClick={::this.open}
      >
        <StyledImage
          src={this.thumbUrl()}
          alt={this.alt()}
        />
        <ul>
          {title && <li>{title}</li>}
          {subtitle && <li>{subtitle}</li>}
        </ul>
      </a>
    )
  }
}

export default CmsContentsImage
