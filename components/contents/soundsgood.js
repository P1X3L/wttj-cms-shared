import React, {Component} from 'react'

class CmsContentsSoundsgood extends Component {

  render() {
    const { content } = this.props
    return (
      <div>
        <iframe
          title={`soundscloud-${content.getIn(['properties', 'playlist_id'])}`}
          src={content.getIn(['properties', 'playlist_url'])}
          frameBorder="0"
          allowFullScreen="true"
        />
      </div>
    )
  }
}

export default CmsContentsSoundsgood
