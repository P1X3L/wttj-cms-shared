import React, {Component} from 'react'

import ArticlesThumb from '../articles/thumb'

class CmsContentsArticles extends Component {

  articles() {
    const { content } = this.props
    return content.getIn(['properties', 'articles'])
  }

  render() {
    return (
      <div>
        {this.articles().map((article, i) =>
          <ArticlesThumb
            key={i}
            article={article}
          />
        )}
      </div>
    )
  }
}

export default CmsContentsArticles
