import React, {Component} from 'react'
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps'

class CmsContentsMap extends Component {

  apiUrl() {
    const { content } = this.props
    return `https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${content.getIn(['properties', 'google_maps_api_key'])}`
  }

  render() {
    const { content } = this.props
    return (
      <div>
        <Map
          googleMapURL={this.apiUrl()}
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `400px` }} />}
          mapElement={<div style={{ height: `100%` }} />}
          lat={content.getIn(['properties', 'headquarter', 'latitude'])}
          lng={content.getIn(['properties', 'headquarter', 'longitude'])}
          defaultZoom={content.getIn(['properties', 'default_zoom'])}
        />
        <ul>
          <li>{content.getIn(['properties', 'headquarter', 'address'])}</li>
          <li>{content.getIn(['properties', 'headquarter', 'zip_code'])} {content.getIn(['properties', 'headquarter', 'city'])}</li>
        </ul>
      </div>
    )
  }
}

const Map = withScriptjs(withGoogleMap((props) =>
  <GoogleMap
    defaultZoom={props.defaultZoom}
    defaultCenter={{ lat: props.lat, lng: props.lng }}
  >
    <Marker position={{ lat: props.lat, lng: props.lng }} />
  </GoogleMap>
))


export default CmsContentsMap
