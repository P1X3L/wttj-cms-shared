import React, {Component} from 'react'

class CmsContentsText extends Component {

  title() {
    const { content } = this.props
    const title = content.getIn(['properties', 'title'])
    if (title) {
      return <p>{title}</p>
    }
  }

  render() {
    const { content } = this.props
    return (
      <article>
        {this.title()}
        {content.getIn(['properties', 'body'])}
      </article>
    )
  }
}

export default CmsContentsText
