import React, {Component} from 'react'

class CmsContentsNumber extends Component {

  render() {
    const { content } = this.props
    return (
      <ul>
        <li>{content.getIn(['properties', 'value'])}</li>
        <li>{content.getIn(['properties', 'label'])}</li>
      </ul>
    )
  }
}

export default CmsContentsNumber
