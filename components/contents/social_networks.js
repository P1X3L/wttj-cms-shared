import React, {Component} from 'react'

class CmsContentsSocialNetworks extends Component {

  networks() {
    const { content } = this.props
    return content.getIn(['properties', 'networks'])
  }

  render() {
    const networks = this.networks()
    return (
      <ul>
        {networks.keySeq().map(k =>
          <li key={k}>
            <a
              href={networks.get(k)}
            >
              {k}
            </a>
          </li>
        )}
      </ul>
    )
  }
}

export default CmsContentsSocialNetworks
