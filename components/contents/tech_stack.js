import React, {Component} from 'react'

class CmsContentsTechStack extends Component {

  thumbUrl(tool) {
    return `${tool.getIn(['image', 'thumb', 'url'])}`
  }

  categories() {
    const { content } = this.props
    return content.getIn(['properties', 'tools'])
      .filter(t => t.get('tools') && t.get('tools').size)
      .sortBy(t => t.get('position'))
  }

  render() {
    return (
      <div>
        {this.categories().map((category, i) =>
          <div key={i}>
            <div>{category.get('name')}</div>
            <ul>
              {category.get('tools').sortBy(t => t.get('position')).map((tool, j) =>
                <li key={j}>
                  <img
                    src={`${this.thumbUrl(tool)}`}
                    alt={tool.get('name')}
                    className='thumb'
                  />
                  {tool.get('name')}
                  {tool.get('percent')}%
                </li>
              )}
            </ul>
          </div>
        )}
      </div>
    )
  }
}

export default CmsContentsTechStack
