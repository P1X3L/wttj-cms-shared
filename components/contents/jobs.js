import React, {Component} from 'react'

import CmsLink from '../link'

class CmsContentsJobs extends Component {

  jobs() {
    const { content } = this.props
    return content.getIn(['properties', 'jobs'])
  }

  render() {
    return (
      <ul>
        {this.jobs().map((job, i) =>
          <li key={i}>
            <CmsLink kind='job' values={{job: job}}>
              <h3>{job.get('name')}</h3>
              <ul>
                <li>{job.get('contract_type')}</li>
                <li>{job.get('city')}</li>
              </ul>
            </CmsLink>
          </li>
        )}
      </ul>
    )
  }
}

export default CmsContentsJobs
