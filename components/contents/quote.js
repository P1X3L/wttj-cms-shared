import React, {Component} from 'react'

class CmsContentsQuote extends Component {

  render() {
    const { content } = this.props
    return (
      <ul>
        <li>{content.getIn(['properties', 'quote'])}</li>
        <li>{content.getIn(['properties', 'name'])}</li>
      </ul>
    )
  }
}

export default CmsContentsQuote
