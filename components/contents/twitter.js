import React, {Component} from 'react'

class CmsContentsTwitter extends Component {
  tweets() {
    const { content } = this.props
    return content.getIn(['properties', 'tweets'])
  }

  screenName() {
    const { content } = this.props
    return `@${content.getIn(['properties', 'screen_name'])}`
  }

  url() {
    const { content } = this.props
    return content.getIn(['properties', 'url'])
  }

  render() {
    return (
      <a
        href={this.url()}
        target='_blank'
      >
        <div>{this.screenName()}</div>
        {this.tweets().map((tweet, i) =>
          <div key={i}>{tweet}</div>
        )}
      </a>
    )
  }
}

export default CmsContentsTwitter
