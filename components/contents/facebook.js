import React, {Component} from 'react'

class CmsContentsFacebook extends Component {
  messages() {
    const { content } = this.props
    return content.getIn(['properties', 'messages'])
  }

  userName() {
    const { content } = this.props
    return `@${content.getIn(['properties', 'username'])}`
  }

  url() {
    const { content } = this.props
    return content.getIn(['properties', 'url'])
  }

  render() {
    return (
      <a
        href={this.url()}
        target='_blank'
      >
        <div>{this.userName()}</div>
        {this.messages().map((message, i) =>
          <div key={i}>{message}</div>
        )}
      </a>
    )
  }
}

export default CmsContentsFacebook
