import React, {Component} from 'react'

class CmsContentsVideo extends Component {

  title() {
    const { content } = this.props
    return content.getIn(['properties', 'title']) || content.getIn(['properties', 'name'])
  }

  subtitle() {
    const { content } = this.props
    return content.getIn(['properties', 'subtitle'])
  }

  thumbUrl() {
    const { content } = this.props
    return content.getIn(['properties', 'image', 'small', 'url'])
  }

  embedUrl() {
    const { content } = this.props
    switch(content.getIn(['properties', 'source'])) {
      case 'youtube':
        return `https://www.youtube.com/embed/${content.getIn(['properties', 'reference'])}?autoplay=0&showinfo=0`
      case '360player':
        return `https://360player.io/p/${content.getIn(['properties', 'reference'])}`
      default:
        return null
    }
  }

  open(e) {
    e && e.preventDefault()
    const { onOpenModal } = this.props
    onOpenModal({
      content: this.renderVideo()
    })
  }

  renderVideo() {
    const { organization } = this.props
    const embedUrl = this.embedUrl()
    const title = this.title()
    const subtitle = this.subtitle()
    return (
      <div>
        {embedUrl &&
          <iframe
            title={title}
            src={embedUrl}
            frameBorder="0"
            allowFullScreen="1"
          />
        }
        <div>{organization && organization.get('name')}</div>
        {title && <div>{title}</div>}
        {subtitle && <div>{subtitle}</div>}
      </div>
    )
  }

  renderInline() {
    return this.renderVideo()
  }

  classNames() {
    let classNames = ['cms-content', 'video']
    return classNames
  }

  renderModal() {
    const title = this.title()
    const subtitle = this.subtitle()
    return (
      <a
        href='#'
        onClick={::this.open}
      >
        <img
          src={this.thumbUrl()}
          alt={this.title()}
        />
        <ul>
          {title && <li>{title}</li>}
          {subtitle && <li>{subtitle}</li>}
        </ul>
      </a>
    )
  }

  render() {
    const { content } = this.props
    switch(content.getIn(['properties', 'display'])) {
      case 'modal':
        return this.renderModal()
      default:
        return this.renderInline()
    }
  }
}

export default CmsContentsVideo
