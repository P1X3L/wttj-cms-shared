import React, {Component} from 'react'

import CmsContentsText from './contents/text'
import CmsContentsImage from './contents/image'
import CmsContentsVideo from './contents/video'
import CmsContentsJobs from './contents/jobs'
import CmsContentsJobsSearch from './contents/jobs_search'
import CmsContentsCompanyStats from './contents/company_stats'
import CmsContentsTechStack from './contents/tech_stack'
import CmsContentsMap from './contents/map'
import CmsContentsTwitter from './contents/twitter'
import CmsContentsFacebook from './contents/facebook'
import CmsContentsSocialNetworks from './contents/social_networks'
import CmsContentsNumber from './contents/number'
import CmsContentsQuote from './contents/quote'
import CmsContentsSoundsgood from './contents/soundsgood'
import CmsContentsArticles from './contents/articles'

export default class CmsBlockContentContainer extends Component {

  renderContent() {
    const { content, organization, contentProps } = this.props
    switch(content.get('kind')) {
      case 'text':
        return <CmsContentsText
          content={content}
          {...contentProps}
        />
      case 'image':
        return <CmsContentsImage
          content={content}
          {...contentProps}
        />
      case 'video':
        return <CmsContentsVideo
          content={content}
          {...contentProps}
        />
      case 'company-stats':
        return <CmsContentsCompanyStats
          content={content}
          {...contentProps}
        />
      case 'jobs':
        return <CmsContentsJobs
          content={content}
          {...contentProps}
        />
      case 'jobs-search':
        return <CmsContentsJobsSearch
          content={content}
          {...contentProps}
        />
      case 'tech-stack':
        return <CmsContentsTechStack
          content={content}
          {...contentProps}
        />
      case 'map':
        return <CmsContentsMap
          content={content}
          {...contentProps}
        />
      case 'twitter':
        return <CmsContentsTwitter
          content={content}
          {...contentProps}
        />
      case 'facebook':
        return <CmsContentsFacebook
          content={content}
          {...contentProps}
        />
      case 'social-networks':
        return <CmsContentsSocialNetworks
          content={content}
          {...contentProps}
        />
      case 'number':
        return <CmsContentsNumber
          content={content}
          {...contentProps}
        />
      case 'quote':
        return <CmsContentsQuote
          content={content}
          {...contentProps}
        />
      case 'soundsgood':
        return <CmsContentsSoundsgood
          content={content}
          {...contentProps}
        />
      case 'articles':
        return <CmsContentsArticles
          content={content}
          {...contentProps}
        />
      default:
        return `UNKNOWN BLOCK ${content.get('kind')}`
    }
  }

  className() {
    const { content } = this.props
    return ['cms-content', content.get('kind')].join(' ')
  }

  render() {
    return (
      <div className={this.className()}>
        {this.renderContent()}
      </div>
    )
  }
}
