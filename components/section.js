import React, {Component} from 'react'
import PropTypes from 'prop-types'

import Container from './container'
import StyledSection from './styled-section'

class Section extends Component {

  // FIXME DRY
  container(layoutContainer) {
    const { section, containers } = this.props
    return containers.find(c =>
      c.get('layout_reference') === layoutContainer.get('reference')
    )
  }

  // FIXME DRY
  renderContainers() {
    const { section, containerComponent } = this.props
    const ContainerComponent = containerComponent
    return section
      .getIn(['layout', 'containers'])
      .sortBy(c => c.get('position'))
      .map((container, i) =>
        <ContainerComponent
          key={i}
          layoutContainer={container}
          container={this.container(container)}
          section={section}
        />
      )
  }

  // FIXME DRY
  styles() {
    const { section } = this.props
    const maxWidth = section.getIn(['properties', 'max-width'])
    let styles = {}
    if (maxWidth) {
      styles['maxWidth'] = maxWidth
    }
    return styles
  }

  render() {
    return (
      <StyledSection
        className='cms-section'
        style={this.styles()}
      >
        {this.renderContainers()}
      </StyledSection>
    )
  }
}

Section.propTypes = {
  containerComponent: PropTypes.func
}

Section.defaultProps = {
  containerComponent: Container
}

export default Section
