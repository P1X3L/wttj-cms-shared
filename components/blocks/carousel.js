import React, {Component} from 'react'

import Content from '../content'

class BlockCarousel extends Component {

  render() {
    const { block, contentProps } = this.props
    return (
      <div>
        {block.getIn(['properties', 'title'])}
        {block.get('contents').map((content, i) =>
          <Content
            key={i}
            content={content}
            contentProps={contentProps}
          />
        )}
      </div>
    )
  }
}

export default BlockCarousel
