import React, {Component} from 'react'

import Content from '../content'

class BlockBasic extends Component {

  renderContents() {
    const { block, contentProps } = this.props
    return block.get('contents').map((content, i) =>
      <Content
        key={i}
        content={content}
        contentProps={contentProps}
      />
    )
  }

  render() {
    return this.renderContents()
  }
}

export default BlockBasic
