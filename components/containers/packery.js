import React, {Component} from 'react'
import PropTypes from 'prop-types'

import Block from '../block'

var Packery = require('react-packery-component')(React)

class CmsContainerGrid extends Component {

  blockStyles(block) {
    return {
      width: block.getIn(['properties', 'width'])
    }
  }

  render() {
    const { container, section, blocks, blockComponent } = this.props
    const BlockComponent = blockComponent
    return (
      <Packery
        className={'packery-container'}
        elementType={'div'}
        options={{
          transitionDuration: '0.5s'
        }}
        disableImagesLoaded={false}
      >
        {blocks.sortBy(b => b.get('position')).map((block, i) =>
          <BlockComponent
            key={i}
            block={block}
            container={container}
            section={section}
            blockStyles={this.blockStyles(block)}
            preventBlink={true}
          />
        )}
      </Packery>
    )
  }
}

CmsContainerGrid.propTypes = {
  blockComponent: PropTypes.func
}

CmsContainerGrid.defaultProps = {
  blockComponent: Block
}

export default CmsContainerGrid
