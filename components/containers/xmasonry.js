import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { XMasonry, XBlock } from 'react-xmasonry/dist/index.js'

import Block from '../block'

class ContainerGrid extends Component {

  blocks() {
    const { container, blocks } = this.props
    if (blocks) { return blocks }
    return container.get('blocks')
  }

  blockWidth(block) {
    const width = block.getIn(['properties', 'width'])
    switch(width) {
      case '33.333333333333336%':
        return 1
      case '66.66666666666667%':
        return 2
      default:
        return 3
    }
  }

  render() {
    const { container, section, blockComponent } = this.props
    const BlockComponent = blockComponent
    return (
      <XMasonry
        maxColumns={3}
        responsive={true}
      >
        {this.blocks().sortBy(b => b.get('position')).map((block, i) =>
          <XBlock
            key={i}
            width={this.blockWidth(block)}
          >
            <BlockComponent
              block={block}
              container={container}
              section={section}
              preventBlink={true}
            />
          </XBlock>
        )}
      </XMasonry>
    )
  }
}

ContainerGrid.propTypes = {
  blockComponent: PropTypes.func
}

ContainerGrid.defaultProps = {
  blockComponent: Block
}

export default ContainerGrid
