import React, {Component} from 'react'
import PropTypes from 'prop-types'

import Block from '../block'

class ContainerStacked extends Component {

  blocks() {
    const { container, blocks } = this.props
    if (blocks) { return blocks }
    return container.get('blocks')
  }

  renderBlocks() {
    const { container, section, blockComponent } = this.props
    const BlockComponent = blockComponent
    return this.blocks().sortBy(b => b.get('position')).map((block, i) =>
      <BlockComponent
        key={i}
        block={block}
        container={container}
        section={section}
      />
    )
  }

  render() {
    return this.renderBlocks()
  }
}

ContainerStacked.propTypes = {
  blockComponent: PropTypes.func
}

ContainerStacked.defaultProps = {
  blockComponent: Block
}

export default ContainerStacked
