import React, {Component} from 'react'
import {Responsive, WidthProvider} from 'react-grid-layout'

import Block from '../block'

const ReactGridLayout = WidthProvider(Responsive)

if (process.env.BUILD_TARGET === 'client') {
  require('react-grid-layout/css/styles.css')
  require('react-resizable/css/styles.css')
}

class CmsContainerGrid extends Component {

  //blockStyles(block) {
    //return {
      //width: block.getIn(['properties', 'width'])
    //}
  //}

  blockWidth(block) {
    const width = block.getIn(['properties', 'width'])
    switch(width) {
      case '33.333333333333336%':
        return 1
      case '66.66666666666667%':
        return 2
      default:
        return 3
    }
  }

  render() {
    const { container, section, blocks } = this.props
    const breakpoints = {lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0}
    let lastX = 0
    let lastY = 0
    const layout = blocks.sortBy(b => b.get('position')).map((block, i) => {
      const width = this.blockWidth(block)
      const height = this.blockWidth(block)
      const b = {
        w: width,
        minW: width,
        h: height,
        x: lastX,
        y: lastY,
        i: `block-${i}`
      }
      let newX = lastX + width
      if (newX >= 3) {
        lastX = 0
        lastY = lastY + 1
      } else {
        lastX = newX
      }
      return b
    }).toJS()
    const layouts = Object.keys(breakpoints).reduce((h, breakpoint) => {
      h[breakpoint] = layout
      return h
    }, {})
    console.log('INITIAL', layouts)
    return (
      <ReactGridLayout
        className="grid-layout"
        layouts={Object.assign({}, layouts)}
        isDraggable={true}
        isResizable={true}
        breakpoints={breakpoints}
        cols={{lg: 3, md: 3, sm: 3, xs: 1, xxs: 1}}
        onBreakpointChange={(newBreakpoint, newCols) => {
          console.log('BreakpointChange', newBreakpoint, newCols)
        }}
        onLayoutChange={(layout, allLayouts) => {
          console.log('LayoutChange', layout, allLayouts)
        }}
      >
        {blocks.sortBy(b => b.get('position')).map((block, i) =>
          <div
            key={`block-${i}`}
          >
            <Block
              block={block}
              container={container}
              section={section}
              setWidth={false}
            />
          </div>
        )}
      </ReactGridLayout>
    )
  }

  //render() {
    //const { container, section, blocks, BlockComponent } = this.props
    //const breakpoints = {lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0}
    //let lastX = 0
    //let lastY = 0
    //const layout = blocks.map((block, i) => {
      //const width = this.blockWidth(block)
      //const height = this.blockWidth(block)
      //const b = {
        //w: width,
        //minW: width,
        //h: height,
        //x: lastX,
        //y: lastY,
        //i: `block-${i}`
      //}
      //let newX = lastX + width
      //if (newX >= 3) {
        //lastX = 0
        //lastY = lastY + 1
      //} else {
        //lastX = newX
      //}
      //return b
    //}).toJS()
    //const layouts = Object.keys(breakpoints).reduce((h, breakpoint) => {
      //h[breakpoint] = layout
      //return h
    //}, {})
    //console.log('INITIAL', layouts)
    //return (
      //<ReactGridLayout
        //className="grid-layout"
        //layouts={Object.assign({}, layouts)}
        //isDraggable={false}
        //isResizable={false}
        //breakpoints={breakpoints}
        //cols={{lg: 3, md: 3, sm: 3, xs: 1, xxs: 1}}
        //onBreakpointChange={(newBreakpoint, newCols) => {
          //console.log('BreakpointChange', newBreakpoint, newCols)
        //}}
        //onLayoutChange={(layout, allLayouts) => {
          //console.log('LayoutChange', layout, allLayouts)
        //}}
      //>
        //{blocks.sortBy(b => b.get('position')).map((block, i) =>
          //<div
            //key={`block-${i}`}
            //style={{
              //background: 'grey'
            //}}
          //>
            //Block {i}
          //</div>
        //)}
      //</ReactGridLayout>
    //)
  //}

  //render() {
    //const layout = {
      //lg: [
        //{w: 2, h: 1, x: 0, y: 0, i: 'block-0'},
        //{w: 1, h: 1, x: 2, y: 0, i: 'block-1'},
        //{w: 1, h: 1, x: 0, y: 1, i: 'block-2'},
        //{w: 1, h: 1, x: 1, y: 1, i: 'block-3'},
        //{w: 1, h: 1, x: 2, y: 1, i: 'block-4'}
      //],
      //xs: [
        //{w: 3, h: 1, x: 0, y: 0, i: 'block-0'},
        //{w: 3, h: 1, x: 0, y: 1, i: 'block-1'},
        //{w: 3, h: 1, x: 0, y: 2, i: 'block-2'},
        //{w: 3, h: 1, x: 0, y: 3, i: 'block-3'},
        //{w: 3, h: 1, x: 0, y: 4, i: 'block-4'}
      //]
    //}
    //console.log('Initial Layout', layout)
    //return (
      //<ReactGridLayout
        //className="grid-layout"
        //layouts={layout}
        //isDraggable={true}
        //isResizable={true}
        //breakpoints={{lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0}}
        //cols={{lg: 3, md: 3, sm: 3, xs: 3, xxs: 1}}
        //onBreakpointChange={(newBreakpoint, newCols) => {
          //console.log('BreakpointChange', newBreakpoint, newCols)
        //}}
        //onLayoutChange={(layout, allLayouts) => {
          //console.log('LayoutChange', layout, allLayouts)
        //}}
      //>
        //<div
          //key={`block-0`}
          //style={{
            //background: 'grey'
          //}}
        //>
          //Block 0
        //</div>
        //<div
          //key={`block-1`}
          //style={{
            //background: 'grey'
          //}}
        //>
          //Block 1
        //</div>
        //<div
          //key={`block-2`}
          //style={{
            //background: 'grey'
          //}}
        //>
          //Block 2
        //</div>
        //<div
          //key={`block-3`}
          //style={{
            //background: 'grey'
          //}}
        //>
          //Block 3
        //</div>
        //<div
          //key={`block-4`}
          //style={{
            //background: 'grey'
          //}}
        //>
          //Block 4
        //</div>
      //</ReactGridLayout>
    //)
  //}
}

export default CmsContainerGrid
