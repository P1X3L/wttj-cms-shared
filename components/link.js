import React, {Component} from 'react'
import Link from 'react-router-dom/Link'

class CmsLink extends Component {

  path() {
    const { kind, values } = this.props
    switch (kind) {
      case 'article':
        return `/articles/${values.slug}`
      case 'job':
        return `/companies/${values.job.get('organizationSlug')}/jobs/${values.job.get('slug')}`
      case 'cms_page':
        const slug = values.page.get('slug')
        return `/companies/${values.organization.get('slug')}${slug ? `/${slug}` : ''}`
    }
  }

  render() {
    const { children } = this.props
    if (process.env.WTTJ_CMS_ENV === 'back') {
      return (
        <a href={this.path()}>
          {children}
        </a>
      )
    } else {
      return (
        <Link to={this.path()}>
          {children}
        </Link>
      )
    }
  }
}

export default CmsLink
