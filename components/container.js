import React, {Component} from 'react'
import PropTypes from 'prop-types'

import ContainerStacked from './containers/stacked'
import ContainerGrid from './containers/grid'
import StyledContainer from './styled-container'

class Container extends Component {

  containerComponent() {
    const { container, containerComponent } = this.props
    if (containerComponent) { return containerComponent }
    switch(container.get('kind')) {
      case 'grid':
        return ContainerGrid
      default:
        return ContainerStacked
    }
  }

  render() {
    const { container, layoutContainer, section, blocks, containerProps, styles } = this.props
    if (!container) { return 'EMPTY CONTAINER'}
    const ContainerComponent = this.containerComponent()
    return (
      <StyledContainer
        className='cms-container'
        container={container}
        layoutContainer={layoutContainer}
        style={styles}
      >
        <ContainerComponent
          container={container}
          section={section}
          blocks={blocks}
          {...containerProps}
        />
      </StyledContainer>
    )
  }
}

Container.propTypes = {
  containerComponent: PropTypes.func
}

export default Container
