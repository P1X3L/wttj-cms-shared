import React, {Component} from 'react'

import BlockBasic from './blocks/basic'
import BlockCarousel from './blocks/carousel'
import StyledBlock from './styled-block'

class Block extends Component {

  className() {
    const { block } = this.props
    return ['cms-block', block.get('kind')].join(' ')
  }

  renderBlock() {
    const { block, contentProps } = this.props
    switch(block.get('kind')) {
      case 'carousel':
        return (
          <BlockCarousel
            block={block}
            contentProps={contentProps}
          />
        )
      default:
        return (
          <BlockBasic
            block={block}
            contentProps={contentProps}
          />
        )
    }
  }

  render() {
    const { block, styles } = this.props
    return (
      <StyledBlock
        className={this.className()}
        data-position={block.get('position')}
        width={block.getIn(['properties', 'width'])}
        style={styles}
      >
        {this.renderBlock()}
      </StyledBlock>
    )
  }
}

export default Block
