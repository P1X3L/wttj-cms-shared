# WTTJ-CMS components

## Develop

To use this package from local source on other applications:

```
cd dev/wttj-cms-shared
npm link
```

```
cd dev/wttj-front
npm link wttj-cms-shared
```
